const express = require('express')
const app = express()
const port = 3000

app.get('/', (req, res) => res.send('Hello World!'))
app.post('/', (req, res) => res.send('Got a POST request'))
app.put('/user', (req, res) => res.send('Got a PUT request at /user'))
app.delete('/user', (req, res) => res.send('Got a DELETE request at /user'))

//To handle Error 404 responses 
app.use((req, res, next) => res.status(404).send("Sorry can't find that"))

//Error handler
app.use ((err, req, res, next) => res.status(500).send("Something broke..."))

app.listen(port, () => console.log(`Example app listening on port ${port}!`))